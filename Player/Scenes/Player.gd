extends KinematicBody2D

const speed = 40
var direction = "down"
var moving = Vector2(0,0)
var UP_VECTOR = Vector2(0, -1)

func _ready():
	pass
	
# warning-ignore:unused_argument
func _process(delta):
	
	if moving.x == 0 and moving.y == 0:
		if direction == "down":
			$AnimationPlayer.play("idle_down")
		if direction == "left":
			$AnimationPlayer.play("idle_left")
		if direction == "up":
			$AnimationPlayer.play("idle_up")
		if direction == "right":
			$AnimationPlayer.play("idle_right")
				
	if Input.is_action_pressed("left"):
		moving.x = -1 * speed
		direction = "left"
	if Input.is_action_pressed("right"):
		moving.x = 1 * speed
		direction = "right"
	if Input.is_action_pressed("up"):
		moving.y = -1 * speed
		direction = "up"
	if Input.is_action_pressed("down"):
		moving.y = 1 * speed
		direction = "down"
		
	if !Input.is_action_pressed("up") and !Input.is_action_pressed("down"):
			moving.y = 0
	if !Input.is_action_pressed("left") and !Input.is_action_pressed("right"):
			moving.x = 0

	moving = move_and_slide(moving, UP_VECTOR)
	set_animation()

func set_animation():
	if moving.x > 0:
		$AnimationPlayer.play("walk_right")
	if moving.x < 0:
		$AnimationPlayer.play("walk_left")
	if moving.y > 0:
		$AnimationPlayer.play("walk_down")
	if moving.y < 0:
		$AnimationPlayer.play("walk_up")
